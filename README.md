# README #

Traffic Signs Detection and Recognition System is an Open Source project, distibuted under the GNU General Public License version 3 (GPL v3), 
you can find more details in file Copying.txt.
For the detection part is used pre-trained model from here: https://github.com/aarcosg/traffic-sign-detection
The recognition model is trained using the code provided in the repository TrafficSignsRecognition: https://bitbucket.org/lachob/trafficsignsrecognition
This repository holds the latest version of the Traffic Signs Detection and Recognition System program source code.

# How to build 
Windows:
g++.exe -w -O3 -std=c++20 -g "path to TrafficSignsSystem.cpp"\**.cpp -o "path where the executable should be" -I C:\OpenCV\include -L C:\OpenCV\x64\mingw\bin -lpthread -lopencv_core452 -lopencv_highgui452 -lopencv_imgcodecs452 -lopencv_imgproc452 -lopencv_dnn452 -lopencv_ml452 -lopencv_video452 -lopencv_videoio452 -lopencv_calib3d452 -lopencv_features2d452 -lopencv_flann452 -I "path to boost 1.80 include" -L "path to boost 1.80 lib" -lboost_system-mgw72-mt-x64-1_80 -lboost_program_options-mgw72-mt-x64-1_80 -lboost_filesystem-mgw72-mt-x64-1_80

Linux:
cmake -S .. -B . -DCMAKE_BUILD_TYPE=Release
make -j 4

# Run
./TSDR -v "path"/some_video.mp4 

Author:
Lachezar Balgariev