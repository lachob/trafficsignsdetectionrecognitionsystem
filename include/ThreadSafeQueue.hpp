#pragma once

#include <queue>
#include <mutex>
#include <optional>
#include <opencv2/core/utility.hpp>

template <typename T>
class ThreadSafeQueue : private std::queue<T> // private inheritance because std::queue<T> has no virtual destructor
{

public:
	using std::queue<T>::push;
	using std::queue<T>::front;
	using std::queue<T>::pop;
	using std::queue<T>::empty;

	ThreadSafeQueue() : counter(0) {}

	void push(const T &entry)
	{
		std::lock_guard<std::mutex> lock(_mutex);

		std::queue<T>::push(entry);
		counter += 1;

		if (counter == 1)
		{
			_tmeter.reset();
			_tmeter.start();
		}
	}

	std::optional<T> get()
	{
		std::lock_guard<std::mutex> lock(_mutex);
		if (!this->empty())
		{
			std::optional<T> entry = std::make_optional<T>(this->front());
			this->pop();
			return entry;
		}
		return std::nullopt;
	}

	float getFPS()
	{
		std::lock_guard<std::mutex> lock(_mutex);
		_tmeter.stop();
		const double fps = counter / _tmeter.getTimeSec();
		_tmeter.start();
		return static_cast<float>(fps);
	}

	bool isEmpty()
	{
		std::lock_guard<std::mutex> lock(_mutex);
		return this->empty();
	}

	void clear()
	{
		std::lock_guard<std::mutex> lock(_mutex);
		while (!this->empty())
		{
			this->pop();
		}
	}

	unsigned int counter;

private:
	cv::TickMeter _tmeter;
	std::mutex _mutex;
};
