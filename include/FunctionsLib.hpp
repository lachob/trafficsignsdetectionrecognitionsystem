#pragma once

#include <csignal>
#include <fstream>
#include <algorithm>
#include <map>
#include <boost/program_options.hpp>
#include <opencv2/videoio.hpp>

#include "ThreadSafeQueue.hpp"

using namespace cv;
namespace po = boost::program_options;
#define SPEED_LIMIT (9)
//#define SPEED_LIMIT  (7)
#define SIGNS_COUNT (3)
#define SPEED_LIMIT_40 (43)
//#define SPEED_LIMIT_40  (20)
/* Set your camera ID here */
constexpr auto camera_id = 0;

class FunctionsLib
{
public:
	static bool isVideoUsed;
	static bool isVideoRecord;

	FunctionsLib(FunctionsLib &other) = delete;
	FunctionsLib(FunctionsLib &&other) = delete;
	~FunctionsLib() = default;
	static const int widthOfDisplaySign = 128;
	constexpr static float inputHeight = 440.0f;
	constexpr static float windowWidth = 800.0f;
	constexpr static float windowHeight = 480.0f + (30.0f * (float(SIGNS_COUNT) - 1));
	static const int saveTimeOfTheRecord = 60;
	static int streamFps;
	static ThreadSafeQueue<Mat> queueFrames;
	static ThreadSafeQueue<Mat> queueFramesProcessed;
	static ThreadSafeQueue<std::vector<Mat>> queuePredictions;
	static volatile sig_atomic_t stop;

	static bool grabFrame(VideoCapture &, Mat &);
	static bool isFileExsist(const char *);
	static bool openSourceStream(VideoCapture &, const std::string &input_src = "");
	static void parseOptionsFromCmd(po::variables_map &, int, char **);
	static std::map<int, std::string> knownSigns;
	static std::string getTrafficSign(int, std::vector<std::string> &, const unsigned int &, bool);
};
