#pragma once

#include <string>
#include <utility>

#include <opencv2/opencv.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/core.hpp>

#include <boost/filesystem.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#include "FunctionsLib.hpp"
#include "ThreadSafeQueue.hpp"

using namespace cv;
namespace fs = boost::filesystem;
using namespace std::chrono;

class DashCamVideoRecorder
{
	std::string directory;
	int vidCodec = VideoWriter::fourcc('M', 'J', 'P', 'G');
	bool isFirstTime = true;
	std::string timeSt;
	VideoWriter videoWriter;

	void record(const Mat &m);

public:
	ThreadSafeQueue<Mat> matrixQueue;
	DashCamVideoRecorder(std::string dir) : directory(std::move(dir))
	{
		if (!fs::is_directory(directory) || !fs::exists(directory))
		{
			fs::create_directory(directory);
		}
	};
	~DashCamVideoRecorder() = default;
	void run();
};
