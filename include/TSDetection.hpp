#pragma once

#include <iostream>
#include <exception>
#include <mutex>

#include <opencv2/core/base.hpp>
#include <opencv2/dnn/dnn.hpp>
#include <opencv2/imgproc.hpp>

#include "FunctionsLib.hpp"

using namespace cv;
using namespace dnn;

class TSDetection
{
	static std::mutex _mutex;
	static Net TSDetection_net_;
	static std::vector<String> frameNames;
	constexpr static float detectSignThrld = 0.90f;
	constexpr static float extendSignPX = 0.008f;
	static const int targetClassID = 44; // background class
	static const int TSDetectionInputWidth = 300;
	static const int TSDetectionInputHeight = 300;

public:
	static bool loadModel();
	static bool detectSign(Mat &, const std::vector<Mat> &, Mat &);
	static void detectionThread();
	TSDetection(TSDetection &other) = delete;
	TSDetection(TSDetection &&other) = delete;
	~TSDetection() = default;
};
