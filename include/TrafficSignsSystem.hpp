#include <thread>
#include <queue>
#include <iostream>

#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <boost/version.hpp>

#include "ThreadSafeQueue.hpp"
#include "FunctionsLib.hpp"
#include "TSDetection.hpp"
#include "TSRecognition.hpp"
#include "VideoStreamInput.hpp"

using namespace cv;
using namespace dnn;

std::map<int, std::string> FunctionsLib::knownSigns = {
    {0, "20"}, {1, "30"}, {2, "50"}, {3, "60"}, {4, "70"}, {5, "80"}, {7, "100"}, {8, "120"}, {9, "No passing"}, {18, "General caution"}, {19, "Dangerouse curve to the left"}, {20, "Dangerouse curve to the right"}, {23, "Slipery road"}, {25, "Road work"}, {27, "Pedestrians"}, {28, "Children crossing"}, {29, "Bicycles crossing"}, {33, "Turn right ahead"}, {35, "Ahead only"}, {36, "Go straight or right"}, {37, "Go straight or left"}, {38, "Keep right"}, {39, "Keep left"}, {40, "Roundabout mandatory"}, {43, "40"}, {44, "No stopping"}};

void TSSRun(VideoCapture &capture, std::unique_ptr<DashCamVideoRecorder> &recorder)
{
    Mat frame, imageForSign(FunctionsLib::widthOfDisplaySign, FunctionsLib::widthOfDisplaySign, CV_8UC3, Scalar::all(255));
    std::string input_fps_str = "vid: 0 FPS", TSDetection_fps_str = "inf: 0 FPS", signLabel = "-";
    std::vector<std::string> lastThreeSigns{SIGNS_COUNT, ""};
    unsigned int index = 0;
    std::cout << "Program started.." << std::endl;

    while (true)
    {
        if (!FunctionsLib::grabFrame(capture, frame))
        {
            break;
        }

        const auto queuePredictionsEl = FunctionsLib::queuePredictions.get();
        if (queuePredictionsEl.has_value())
        {
            /* Check if a sign was detected sucessfully */
            std::vector<Mat> outs = queuePredictionsEl.value();
            auto pred_frame = FunctionsLib::queueFramesProcessed.get();
            Mat signToClassify;
            bool isFound = false;
            if (pred_frame.has_value())
            {
                isFound = TSDetection::detectSign(pred_frame.value(), outs, signToClassify);
            }

            if (isFound)
            {
                TSRecognition::recognizeSign(signToClassify, imageForSign, signLabel, lastThreeSigns, index, isFound);
            }

            TSDetection_fps_str = format("inf:%dFPS", int(FunctionsLib::queuePredictions.getFPS()));
        }

        /* Draw the UI and show it on the display */
        Rect roi_sign(Point(0, frame.size().height - FunctionsLib::widthOfDisplaySign), imageForSign.size());
        imageForSign.copyTo(frame(roi_sign));
        int x_add = int((FunctionsLib::windowWidth - frame.size().width) / 2);
        Rect roi_frame(Point(x_add, 0), frame.size());
        Mat display_frame(int(FunctionsLib::windowHeight), int(FunctionsLib::windowWidth), CV_8UC3, Scalar::all(255));
        frame.copyTo(display_frame(roi_frame));
        input_fps_str = format("vid:%dFPS", int(FunctionsLib::queueFrames.getFPS()));

        putText(display_frame, signLabel, Point(x_add, frame.size().height + 25), FONT_HERSHEY_COMPLEX_SMALL, 1, Scalar::all(0));
        putText(display_frame, input_fps_str, Point(x_add + 545, frame.size().height + 25), FONT_HERSHEY_COMPLEX_SMALL, 1, Scalar::all(0));
        putText(display_frame, TSDetection_fps_str, Point(x_add + 665, frame.size().height + 25), FONT_HERSHEY_COMPLEX_SMALL, 1, Scalar::all(0));
        imshow("Traffic Signs Detection and Recognition System", display_frame);

        /* Send frame to video recorder */
        if (FunctionsLib::isVideoRecord)
        {
            recorder->matrixQueue.push(display_frame);
        }

        if (waitKey(1) > 0)
        {
            std::cout << "User key pressed, exit!" << std::endl;
            break;
        }
    }
}

bool loadModels(int ac, char **av)
{
    bool isLoaded = true;
    /* Check the necessary installed versions */
    std::cout << "OpenCV version: " << CV_VERSION << std::endl;
    std::cout << "Boost version: " << BOOST_VERSION / 100000 << "." << BOOST_VERSION / 100 % 1000 << "." << BOOST_VERSION % 100 << std::endl;
    std::cout << "Traffic Signs Detection and Recognition System started..." << std::endl;

    /* Load the models */
    if (!TSDetection::loadModel() || !TSRecognition::loadModel())
    {
        std::cerr << "Error loading required models, exit.." << std::endl;
        isLoaded = false;
    }
    return isLoaded;
}

bool openInput(VideoCapture &capture, int ac, char **av)
{
    po::variables_map vm;
    FunctionsLib::parseOptionsFromCmd(vm, ac, av);
    bool isSuccess = false;
    if (FunctionsLib::isVideoUsed)
    {
        std::string pathToVideo = vm["video"].as<std::string>();
        std::cout << "Opening video file: " << pathToVideo << ".." << std::endl;
        if (!FunctionsLib::isFileExsist(pathToVideo.c_str()))
        {
            std::cerr << "Input video does not exist, exit the program!" << std::endl;
            isSuccess = false;
        }
        else
            isSuccess = FunctionsLib::openSourceStream(capture, pathToVideo);
    }
    else
    {
        std::cout << "Opening camera.." << std::endl;
        isSuccess = FunctionsLib::openSourceStream(capture);
    }
    return isSuccess;
}
