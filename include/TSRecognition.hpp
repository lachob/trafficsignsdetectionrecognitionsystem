#pragma once

#include <algorithm>
#include <mutex>

#include <opencv2/opencv.hpp>
#include <opencv2/dnn/dnn.hpp>
#include <opencv2/imgproc.hpp>

#include "FunctionsLib.hpp"

using namespace cv;
using namespace dnn;

class TSRecognition
{
	static std::mutex _mutex;
	static Net TSRecognition_NN_net;
	static Net TSRecognition_NN_net_2;
	constexpr static double recognizeSignThrld = 0.99f;
	constexpr static double recognizeSignThrld_40 = 0.99f;
	static const int inputResolutionPX = 32;

public:
	static bool loadModel();
	static void recognizeSign(const Mat &, Mat &, std::string &, std::vector<std::string> &, unsigned int &, bool);
	TSRecognition(TSRecognition &other) = delete;
	TSRecognition(TSRecognition &&other) = delete;
	~TSRecognition() = default;
};
