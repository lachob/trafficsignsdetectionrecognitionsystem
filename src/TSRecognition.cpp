

#include "TSRecognition.hpp"

std::mutex TSRecognition::_mutex;
Net TSRecognition::TSRecognition_NN_net;
Net TSRecognition::TSRecognition_NN_net_2;

/* Path of the model is defined here */
auto TSRecognition_model_path = ("../models/classification_2.pb");
auto TSRecognition_model_path_2 = ("../models/classification_1.pb");

bool TSRecognition::loadModel() {
    if ((!FunctionsLib::isFileExsist(TSRecognition_model_path)) || (!FunctionsLib::isFileExsist(TSRecognition_model_path))) {
        return false;
    }

    TSRecognition_NN_net = readNetFromTensorflow(TSRecognition_model_path, "");
    TSRecognition_NN_net.setPreferableBackend(DNN_BACKEND_DEFAULT);
    TSRecognition_NN_net.setPreferableTarget(DNN_TARGET_CPU);

    TSRecognition_NN_net_2 = readNetFromTensorflow(TSRecognition_model_path_2, "");
    TSRecognition_NN_net_2.setPreferableBackend(DNN_BACKEND_DEFAULT);
    TSRecognition_NN_net_2.setPreferableTarget(DNN_TARGET_CPU);

    return true;
}

void TSRecognition::recognizeSign(const Mat &signToClassify, Mat &imageForSign, std::string &signLabel, std::vector<std::string> &lastThreeSigns, unsigned int &index, bool detected) {
    std::string s = "";
    bool isFound = true;
    bool isSL_40 = false;
    int class_id = 50;
    /* Downsize image to FunctionsLib::inputResolutionPX and make it grayscale */
    Mat blob;
    ++index;
    if (index >= SIGNS_COUNT) {
        index = 0;
    }
    if (detected) {
        cvtColor(signToClassify, blob, COLOR_BGR2GRAY);
        double confidence = 0, confidence_2 = 0;
        resize(blob, blob, Size(inputResolutionPX, inputResolutionPX), INTER_AREA);
        // equalize the histogram
        Mat hist_equalized_image_2;
        equalizeHist(blob, hist_equalized_image_2);
        Mat normalizedImage;
        hist_equalized_image_2.convertTo(normalizedImage, CV_32FC3, 1.f / 255);
        blobFromImage(normalizedImage, normalizedImage, 1.0, Size(inputResolutionPX, inputResolutionPX), Scalar(), false, false);

        TSRecognition_NN_net_2.setInput(normalizedImage);
        Mat prob_2 = TSRecognition_NN_net_2.forward();
        Point class_id_point_2;
        minMaxLoc(prob_2.reshape(1, 1), nullptr, &confidence_2, nullptr, &class_id_point_2);
        class_id = class_id_point_2.x;
        if ((class_id != SPEED_LIMIT_40) || (class_id == SPEED_LIMIT_40 && confidence_2 <= recognizeSignThrld_40)) {
            TSRecognition_NN_net.setInput(normalizedImage);
            Mat prob = TSRecognition_NN_net.forward();
            Point class_id_point;
            minMaxLoc(prob.reshape(1, 1), nullptr, &confidence, nullptr, &class_id_point);
            class_id = class_id_point.x;
            isSL_40 = false;
        } else {
            isSL_40 = true;
            confidence = confidence_2;
        }

        std::cout << ", Classification: " << class_id << ", Confidence: " << confidence << std::endl;

        if (isSL_40)
            imageForSign = signToClassify;
        else {
            if ((confidence > recognizeSignThrld) && (FunctionsLib::knownSigns.find(class_id) != FunctionsLib::knownSigns.end()) && detected) {
                imageForSign = signToClassify;
            } else {
                isFound = false;
            }
        }
    } else {
        isFound = false;
    }

    s = FunctionsLib::getTrafficSign(class_id, lastThreeSigns, index, isFound);
    signLabel = (s != "-" ? s : signLabel);
}
