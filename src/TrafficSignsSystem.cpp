
#include "TrafficSignsSystem.hpp"

int main(int ac, char** av)
{

	bool isLoaded = loadModels(ac, av);
	/* Open the input file/stream */
	VideoCapture capture;
	bool isSuccess = openInput(capture, ac, av);
	if ((!isLoaded) || (!isSuccess))
	{
		exit(EXIT_FAILURE);
	}

	/* Prepare all threads */
	std::unique_ptr<DashCamVideoRecorder> recorder;
	FunctionsLib::stop = false;
	std::vector<std::thread> threads;
	if (FunctionsLib::isVideoRecord)
	{
		recorder = std::make_unique<DashCamVideoRecorder>("./record/");
		threads.emplace_back(std::thread(&DashCamVideoRecorder::run, recorder.get()));
	}
	threads.emplace_back(TSDetection::detectionThread);

	/* Run the main program */
	TSSRun(capture, recorder);

	FunctionsLib::stop = true;
	/* Start all threads */
	for (auto& th : threads)
	{
		th.join();
	}
	/* free all allocated resources */
	destroyAllWindows();
	exit(EXIT_SUCCESS);
}
