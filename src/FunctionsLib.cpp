#include <iostream>
#include <opencv2/videoio.hpp>
#include <opencv2/imgproc.hpp>
#include "FunctionsLib.hpp"

ThreadSafeQueue<Mat> FunctionsLib::queueFrames;
ThreadSafeQueue<Mat> FunctionsLib::queueFramesProcessed;
ThreadSafeQueue<std::vector<Mat>> FunctionsLib::queuePredictions;
volatile sig_atomic_t FunctionsLib::stop;
bool FunctionsLib::isVideoUsed = false;
bool FunctionsLib::isVideoRecord = false;
int FunctionsLib::streamFps = 0;

bool FunctionsLib::grabFrame(VideoCapture &capture, Mat &frame)
{
    /* Grab frame and add it to the frame queue */
    capture >> frame;
    if (!frame.empty())
    {
        FunctionsLib::queueFrames.push(frame.clone());
    }
    else if (FunctionsLib::isVideoUsed)
    {
        std::cout << "End of video reached, exit.." << std::endl;
        return false;
    }
    else
    {
        std::cerr << "Error connecting to camera, exit.." << std::endl;
        return false;
    }

    /* Downsize input image for display */
    const float ratio = FunctionsLib::inputHeight / frame.size().height;
    resize(frame, frame, Size(), ratio, ratio, INTER_LINEAR);
    return true;
}

bool FunctionsLib::isFileExsist(const char *fileName)
{
    std::ifstream infile(fileName);
    return infile.good();
}

bool FunctionsLib::openSourceStream(VideoCapture &capture, const std::string &inpSrc)
{
    try
    {
        /* Open Webcam */
        if (inpSrc.empty())
        {
            capture.open(camera_id);
            streamFps = int(capture.get(CAP_PROP_FPS));
            int cameraWidth = int(capture.get(CAP_PROP_FRAME_WIDTH));
            int cameraHeight = int(capture.get(CAP_PROP_FRAME_HEIGHT));
            std::cout << "Opened camera with resolution " << cameraWidth << "x" << cameraHeight << " and FPS: " << streamFps << ".." << std::endl;
        }

        /* Open Video */
        else
        {
            capture.open(inpSrc);
            int videoWidth = int(capture.get(CAP_PROP_FRAME_WIDTH));
            int videoHeight = int(capture.get(CAP_PROP_FRAME_HEIGHT));
            std::cout << "Opened video with resolution " << videoWidth << "x" << videoHeight << ".." << std::endl;
        }
    }
    catch (...)
    {
        std::cerr << "Unable to open our input source, exit!" << std::endl;
        return false;
    }

    if (!capture.isOpened())
    {
        std::cerr << "Unable to open our input source, exit!" << std::endl;
        return false;
    }

    return true;
}

void FunctionsLib::parseOptionsFromCmd(po::variables_map &vm, int ac, char **av)
{
    try
    {
        po::options_description desc("Allowed options");
        desc.add_options()("video,v", po::value<std::string>(), "use test video instead of camera stream")("record,r", "record output to video file");

        store(parse_command_line(ac, av, desc), vm);
        notify(vm);

        /* Use test video instead of camera stream */
        if (vm.count("video"))
        {
            std::cout << "Using video instead of camera stream.." << std::endl;
            isVideoUsed = true;
        }

        /* Record display to video file */
        if (vm.count("record"))
        {
            std::cout << "Recording video with resolution - " << windowWidth << "x" << windowHeight << " and " << streamFps << " FPS.." << std::endl;
            isVideoRecord = true;
        }
    }
    catch (boost::program_options::error &e)
    {
        std::cerr << "ERROR: " << e.what() << std::endl;
        exit(EXIT_FAILURE);
    }
}

std::string FunctionsLib::getTrafficSign(const int class_id, std::vector<std::string> &lastThreeSigns, const unsigned int &index, bool isFound)
{
    if (isFound)
    {
        const std::string sign = knownSigns[class_id];
        std::string result_str = "";
        if ((class_id == SPEED_LIMIT_40) || (class_id < SPEED_LIMIT))
        {
            result_str = "Limit: " + sign + " km/h";
            return result_str;
        }
        else
        {
            return sign;
        }
        // if ((class_id <= SPEED_LIMIT) || (class_id == SPEED_LIMIT_40))
        // {
        // 	if (sign != "0") {
        // 		result_str = "Limit: " + sign + " km/h";
        // 		if (std::find(lastThreeSigns.begin(), lastThreeSigns.end(), result_str) == lastThreeSigns.end())
        // 			lastThreeSigns[index] = result_str;
        // 		return result_str;
        // 	}
        // 	else {
        // 		result_str = "";
        // 		lastThreeSigns[index] = result_str;
        // 		return result_str;
        // 	}
        // }
        // else {
        // 	auto it = std::find(lastThreeSigns.begin(), lastThreeSigns.end(), sign);
        // 	if (it == lastThreeSigns.end())
        // 		lastThreeSigns[index] = sign;

        // 	return "-";
        // }
    }
    else
    {
        //lastThreeSigns[index] = "";
        return "-";
    }

    //return "Limit: - km/h";
}