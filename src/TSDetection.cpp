#include "TSDetection.hpp"

std::mutex TSDetection::_mutex;
Net TSDetection::TSDetection_net_;
std::vector<String> TSDetection::frameNames;

/* Path of the model is defined here */
constexpr auto TSDetection_model_path = ("../models/detection.pb");
constexpr auto TSDetection_config_path = ("../models/detection.pbtxt");

bool TSDetection::loadModel() {
    if (!FunctionsLib::isFileExsist(TSDetection_model_path) || !FunctionsLib::isFileExsist(TSDetection_config_path)) {
        return false;
    }
    TSDetection_net_ = readNetFromTensorflow(TSDetection_model_path, TSDetection_config_path);
    TSDetection_net_.setPreferableBackend(DNN_BACKEND_DEFAULT);
    TSDetection_net_.setPreferableTarget(DNN_TARGET_CPU);
    frameNames = TSDetection_net_.getUnconnectedOutLayersNames();

    return true;
}

bool TSDetection::detectSign(Mat &frame, const std::vector<Mat> &outs, Mat &imageForSign) {
    for (const auto &out : outs) {
        const auto data = reinterpret_cast<float *>(out.data);
        for (size_t i = 0; i < out.total(); i += 7) {
            const float confidence = data[i + 2];
            const int class_id = static_cast<int>(data[i + 1]) - 1;

            /* Check if threshold is reached and target class is not background */
            if ((confidence >= detectSignThrld) && (class_id < targetClassID)) {
                const float left = data[i + 3];
                const float top = data[i + 4];
                const float right = data[i + 5];
                const float bottom = data[i + 6];

                const int imageWidth = frame.size().width;
                const int imageHeight = frame.size().height;

                const auto boxLeft = static_cast<int>(std::max(int((left - extendSignPX) * imageWidth), 0));
                const auto boxTop = static_cast<int>(std::max(int((top - extendSignPX) * imageHeight), 0));
                const auto boxRight = static_cast<int>(std::min(int((right + extendSignPX) * imageWidth), imageWidth - 1));
                const auto boxBottom = static_cast<int>(std::min(int((bottom + extendSignPX) * imageHeight), imageHeight - 1));

                if (boxLeft >= 0 && boxTop >= 0 && boxRight >= 0 && boxBottom >= 0) {
                    /* Cut out the sign and resize it for eventual display over the video/stream */
                    try {
                        imageForSign = frame(Rect(boxLeft, boxTop, abs(boxRight - boxLeft), abs(boxBottom - boxTop))).clone();
                        resize(imageForSign, imageForSign, Size(FunctionsLib::widthOfDisplaySign, FunctionsLib::widthOfDisplaySign), INTER_AREA);
                        return true;
                    } catch (std::exception &ex) {
                        std::cout << "Error: Outside box boundaries " << std::endl;
                    }
                } else {
                    return false;
                }
            }
        }
    }
    return false;
}

void TSDetection::detectionThread() {
    Mat blob;
    std::cout << "Processing Thread started.." << std::endl;
    while (!FunctionsLib::stop) {
        auto frame = FunctionsLib::queueFrames.get();
        {
            if (frame.has_value()) {
                FunctionsLib::queueFrames.clear();
            }
        }

        /* Start the inference */
        if (frame.has_value()) {
            blobFromImage(frame.value(), blob, 1.0, Size(TSDetectionInputWidth, TSDetectionInputHeight), Scalar(), true);
            TSDetection_net_.setInput(blob);

            std::vector<Mat> outs;
            TSDetection_net_.forward(outs, frameNames);
            FunctionsLib::queuePredictions.push(outs);
            FunctionsLib::queueFramesProcessed.push(frame.value());
        }
    }
}