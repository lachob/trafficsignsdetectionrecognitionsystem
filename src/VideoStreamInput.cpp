#include "VideoStreamInput.hpp"

void DashCamVideoRecorder::record(const Mat &m)
{
    static auto sTimer = high_resolution_clock::now();

    if (!m.empty())
    {
        if (isFirstTime)
        {
            timeSt = to_iso_string(boost::posix_time::second_clock::local_time());
            std::cout << "First video file is created: " << directory << timeSt << ".avi" << std::endl;
            videoWriter.open(directory + timeSt + ".avi", vidCodec, FunctionsLib::streamFps, Size(int(FunctionsLib::windowWidth), int(FunctionsLib::windowHeight)), true);
            isFirstTime = false;
        }

        Mat frame_out;
        resize(m, frame_out, Size(int(FunctionsLib::windowWidth), int(FunctionsLib::windowHeight)));
        videoWriter.write(frame_out);

        if (duration_cast<seconds>(high_resolution_clock::now() - sTimer).count() >= FunctionsLib::saveTimeOfTheRecord)
        {
            timeSt = to_iso_string(boost::posix_time::second_clock::local_time());
            std::cout << "Saving video to file: " << directory << timeSt << ".avi" << std::endl;
            videoWriter.open(directory + timeSt + ".avi", vidCodec, FunctionsLib::streamFps, Size(int(FunctionsLib::windowWidth), int(FunctionsLib::windowHeight)), true);

            sTimer = high_resolution_clock::now();
        }
    }
}

void DashCamVideoRecorder::run()
{
    try
    {
        while (!FunctionsLib::stop)
        {
            auto t = matrixQueue.get();
            if (t.has_value())
            {
                record(t.value());
            }
        }
    }
    catch (...)
    {
    }
}